# Prerequisites
Install following packages first:
> sudo apt install ansible git

# Lessons Learned

The powerline galaxy role will pull a git repo to /tmp. If you have accidently done this with "become: true", you will not be able to do this as a normal user afther this run. You will get a "permnission denied" error. Fix it with removing the repo in /tmp as root.

Even if your role or playbook don't need sudo access, an included galaxy role, such as oh-my-zsh, might require it indeed. The same counts for "gather_facts: yes". If you don't set gather_facts to yes, the zsh module will fail "wrong value for become, line ansible_distribition".


# Example code
https://medium.com/splunkuserdeveloperadministrator/using-ansible-pull-in-ansible-projects-ac04466643e8

# Use playbooks
Provision the workstation with apt packages:
> ansible-pull -U https://gitlab.com/brunodooms/ansible-pull-demo -i hosts --ask-become-pass packages.yml

Install oh-my-zsh. Note: needs a sudo password and "gather facts" set to yes!
> ansible-pull -U https://gitlab.com/brunodooms/ansible-pull-demo -i hosts --ask-become-pass oh-my-zsh.yml

Upgrade the workstation:
> ansible-pull -U https://gitlab.com/brunodooms/ansible-pull-demo -i hosts --ask-become-pass upgrade.yml

# Use roles
Use all roles:
> time ansible-pull -U https://gitlab.com/brunodooms/ansible-pull-demo -i hosts --ask-become-pass main.yml

Upgrade:
> time ansible-pull -U https://gitlab.com/brunodooms/ansible-pull-demo -i hosts --ask-become-pass main.yml --tags up

Use a single role, e.g. oh-my-zsh
> time ansible-pull -U https://gitlab.com/brunodooms/ansible-pull-demo -i hosts --ask-become-pass main.yml --tags zsh
